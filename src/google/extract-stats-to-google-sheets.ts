import { Arguments } from 'yargs';

import { SharedCLIOptions } from '../cli-options';
import { getSheetDefinitionsFromTimeRange } from '../gitlab';
import { createSpreadsheet } from './Spreadsheet';
import { createGoogleAuth } from './auth';

export function extractStatsToGoogleSheets(args: Arguments<SharedCLIOptions>) {
  main(args).then(
    (spreadsheetUrl) => {
      console.log('Google Sheet URL:', spreadsheetUrl);
    },
    (error) => {
      console.error(error.message);
    },
  );
}

async function main({
  fromDate,
  toDate,
  ignoreUserNames,
}: Arguments<SharedCLIOptions>) {
  const auth = await createGoogleAuth();

  const sheetDefinitions = getSheetDefinitionsFromTimeRange(
    fromDate,
    toDate,
    ignoreUserNames,
  );

  const spreadsheet = await createSpreadsheet(
    auth,
    `Stats ${fromDate.format()} - ${toDate.format()}`,
    sheetDefinitions,
  );

  await spreadsheet.import();

  return spreadsheet.getSpreadsheetUrl();
}
