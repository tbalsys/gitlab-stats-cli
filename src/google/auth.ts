import { google } from 'googleapis';
import { JWT } from 'google-auth-library';

const SCOPES = ['https://www.googleapis.com/auth/drive'];

export const createGoogleAuth = (): Promise<JWT> =>
  new Promise((resolve, reject) => {
    console.log('Authenticating with Google API');
    if (!process.env.GOOGLE_PRIVATE_KEY) {
      throw new Error('Environment variable GOOGLE_PRIVATE_KEY is not set');
    }

    const jwtClient = new google.auth.JWT(
      process.env.GOOGLE_CLIENT_EMAIL,
      undefined,
      process.env.GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n'),
      SCOPES,
    );

    jwtClient.authorize((err) => {
      if (err) {
        reject(err);
      } else {
        resolve(jwtClient);
      }
    });
  });
