import { drive_v3, google, sheets_v4 } from 'googleapis';
import { OAuth2Client } from 'googleapis-common';
import { Observable, from } from 'rxjs';
import { StatsTable, SheetDefinition } from '../gitlab/types';
import { toCsv } from '../gitlab/operators/toCsv';
import { map, mergeMap } from 'rxjs/operators';

class Spreadsheet {
  private api: sheets_v4.Sheets;
  private spreadsheet: sheets_v4.Schema$Spreadsheet;
  private statStreams: Observable<{
    sheetId: number;
    title: string;
    stream: Observable<StatsTable>;
  }>;

  constructor(
    api: sheets_v4.Sheets,
    data: sheets_v4.Schema$Spreadsheet,
    sheetStreams: SheetDefinition[],
  ) {
    this.api = api;
    this.spreadsheet = data;
    if (data.sheets) {
      const sheetIds = [];
      for (let i = 0; i < data.sheets.length; ++i) {
        const properties = data.sheets[i].properties;
        if (properties) {
          sheetIds.push({
            ...sheetStreams[i],
            sheetId: properties.sheetId as number,
          });
        } else {
          throw new Error('Sheed id missing in response from Google API');
        }
      }
      this.statStreams = from(sheetIds);
    } else {
      throw new Error('No sheets in response from Google API');
    }
  }

  public getSpreadsheetUrl() {
    return this.spreadsheet.spreadsheetUrl;
  }

  public import = () =>
    new Promise((resolve, reject) => {
      this.statStreams
        .pipe(
          mergeMap(({ sheetId, title, stream }) =>
            stream.pipe(
              toCsv(),
              map(async ({ csv, length }) => {
                await this.importCsv(sheetId, csv);
                await this.addChart(sheetId, title, length);
              }),
            ),
          ),
        )
        .subscribe({
          error: (error) => {
            reject(error);
          },
          complete: () => {
            resolve();
          },
        });
    });

  private importCsv = (sheetId: number, data: string) =>
    new Promise((resolve, reject) => {
      const request: sheets_v4.Params$Resource$Spreadsheets$Batchupdate = {
        spreadsheetId: this.spreadsheet.spreadsheetId as string,

        requestBody: {
          requests: [
            {
              pasteData: {
                coordinate: {
                  sheetId,
                  rowIndex: 0,
                  columnIndex: 0,
                },
                delimiter: ',',
                data,
              },
            },
          ],
        },
      };

      this.api.spreadsheets.batchUpdate(request, (err) => {
        if (err) {
          reject(err);
        }

        resolve();
      });
    });

  private addChart = (sheetId: number, title: string, length: number) =>
    new Promise((resolve, reject) => {
      const request: sheets_v4.Params$Resource$Spreadsheets$Batchupdate = {
        spreadsheetId: this.spreadsheet.spreadsheetId as string,

        requestBody: {
          requests: [
            {
              addChart: {
                chart: {
                  spec: {
                    title,
                    basicChart: {
                      chartType: 'BAR',
                      axis: [
                        {
                          position: 'BOTTOM_AXIS',
                          title: 'Count',
                        },
                        {
                          position: 'LEFT_AXIS',
                          title: 'User',
                        },
                      ],
                      domains: [
                        {
                          domain: {
                            sourceRange: {
                              sources: [
                                {
                                  sheetId,
                                  startRowIndex: 0,
                                  endRowIndex: length,
                                  startColumnIndex: 0,
                                  endColumnIndex: 1,
                                },
                              ],
                            },
                          },
                        },
                      ],
                      series: [
                        {
                          series: {
                            sourceRange: {
                              sources: [
                                {
                                  sheetId,
                                  startRowIndex: 0,
                                  endRowIndex: length,
                                  startColumnIndex: 1,
                                  endColumnIndex: 2,
                                },
                              ],
                            },
                          },
                        },
                      ],
                      headerCount: 1,
                    },
                  },
                  position: {
                    overlayPosition: {
                      anchorCell: {
                        sheetId,
                        rowIndex: 0,
                        columnIndex: 3,
                      },
                      heightPixels: 770,
                      widthPixels: 570,
                    },
                  },
                },
              },
            },
          ],
        },
      };

      this.api.spreadsheets.batchUpdate(request, (err) => {
        if (err) {
          reject(err);

          return;
        }

        resolve();
      });
    });
}

export const createSpreadsheet = (
  auth: OAuth2Client,
  spreadsheetTitle: string,
  sheetStreams: SheetDefinition[],
): Promise<Spreadsheet> =>
  new Promise((resolve, reject) => {
    console.log('Creating Google spreadsheet');

    const params: sheets_v4.Params$Resource$Spreadsheets$Create = {
      requestBody: {
        properties: {
          title: spreadsheetTitle,
        },
        sheets: sheetStreams.map(({ title }) => ({
          properties: {
            title,
          },
        })),
      },
      fields: 'spreadsheetId,spreadsheetUrl,sheets.properties.sheetId',
    };

    const sheets = google.sheets({ version: 'v4', auth });
    sheets.spreadsheets.create(params, (createError, spreadsheet) => {
      if (createError) {
        reject(createError);
      } else if (spreadsheet && spreadsheet.data) {
        const data = spreadsheet.data;
        if (data.spreadsheetId) {
          const spreadsheetId = data.spreadsheetId;
          const move: drive_v3.Params$Resource$Files$Update = {
            addParents: process.env.GOOGLE_PARENT_FOLDER_ID,
            removeParents: 'root',
            fileId: spreadsheetId,
          };

          const drive = google.drive({ version: 'v3', auth });
          drive.files.update(move, (moveError) => {
            if (moveError) {
              reject(moveError);
            } else {
              resolve(new Spreadsheet(sheets, data, sheetStreams));
            }
          });
        } else {
          reject(new Error('No spreadsheetId received'));
        }
      } else {
        reject(new Error('No spreadsheet data received'));
      }
    });
  });
