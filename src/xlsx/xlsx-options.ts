import { Argv } from 'yargs';
import { SharedCLIOptions, sharedCLIOptionsBuilder } from '../cli-options';

export interface XLSXClIOptions extends SharedCLIOptions {
  outputFilePath: string;
}

export const xlsxCLIOptionsBuilder = (options: Argv) =>
  sharedCLIOptionsBuilder(options).option('file', {
    alias: ['o', 'outputFilePath'],
    defaultDescription: 'stats-[fromDate]-[toDate].xlsx',
    description: 'Path to the output XLSX file that will be created',
  });
