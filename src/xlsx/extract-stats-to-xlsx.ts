import { Arguments } from 'yargs';
import * as XLSX from 'xlsx';
import { getSheetDefinitionsFromTimeRange } from '../gitlab';
import { XLSXClIOptions } from './xlsx-options';

export function extractStatsToXlsx(args: Arguments<XLSXClIOptions>) {
  main(args)
    .then((outputFilePath) => console.log('Stats saved to', outputFilePath))
    .catch((error) => console.error(error.message));
}

async function main({
  fromDate,
  toDate,
  outputFilePath = `stats-${fromDate.format()}-${toDate.format()}.xlsx`,
  ignoreUserNames,
}: Arguments<XLSXClIOptions>) {
  const workbook = XLSX.utils.book_new();

  const sheetsDefinition = getSheetDefinitionsFromTimeRange(
    fromDate,
    toDate,
    ignoreUserNames,
  );

  const worksheets = await Promise.all(
    sheetsDefinition.map(
      async ({ stream, title, countColumnTitle: numberTitle }) => {
        const statsTable = await stream.toPromise();
        const worksheetData = [
          ['User', numberTitle],
          ...statsTable.map(({ User, Count }) => [User, Count]),
        ];

        const worksheet = XLSX.utils.aoa_to_sheet(worksheetData);

        return { title, worksheet };
      },
    ),
  );

  worksheets.forEach(({ worksheet, title }) => {
    XLSX.utils.book_append_sheet(workbook, worksheet, title);
  });

  XLSX.writeFile(workbook, outputFilePath);

  return outputFilePath;
}
