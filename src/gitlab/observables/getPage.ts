import { from } from 'rxjs';
import { getPage } from '../api/getPage';
export const getPage$ = (url: string) => from(getPage(url));
