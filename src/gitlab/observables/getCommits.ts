import { Moment } from 'moment';
import { map, concatAll } from 'rxjs/operators';
import { getAllPages$ } from './getPages';
import { toStatsTable } from '../operators/toStatsTable';

export const getCommits$ = (createdAfter: Moment, createdBefore: Moment) =>
  getAllPages$({
    method: 'GET',
    url: `/projects/${process.env.GITLAB_PROJECT_ID}/repository/commits`,
    params: {
      since: createdAfter.toISOString(),
      until: createdBefore.toISOString(),
      per_page: 100,
    },
  }).pipe(
    concatAll(),
    map((commit) => commit.author_name),
    toStatsTable(),
  );
