import { AxiosRequestConfig } from 'axios';
import { EMPTY } from 'rxjs';
import { expand, map } from 'rxjs/operators';
import axios from '../api/axios';
import { getPage$ } from './getPage';

export const getAllPages$ = (requestParams: AxiosRequestConfig) =>
  getPage$(axios.getUri(requestParams)).pipe(
    expand(({ next }) => (next ? getPage$(next) : EMPTY)),
    map(({ data }) => data),
  );
