import { MergeRequest } from '../types';

export const debugMr = (mr: MergeRequest) =>
  console.log({
    iid: mr.iid,
    author: mr.author.name,
    merged_by: mr.merged_by && mr.merged_by.name,
    assignee: mr.assignee && mr.assignee.name,
    approvers: mr.approvers.map((item) => item.user.name),
    assignees: mr.assignees.map((item) => item.name),
    suggested_approvers: mr.suggested_approvers.map((item) => item.name),
    approved_by: mr.approved_by.map((item) => item.user.name),
    award_emoji: mr.award_emoji.map((item) => item.user.name),
    discussions: mr.discussions.map((item) =>
      item.notes.map((note) => note.author.name),
    ),
  });
