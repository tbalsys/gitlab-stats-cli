import { Moment } from 'moment';
import { Observable } from 'rxjs';
import { map, concatAll, concatMap, share } from 'rxjs/operators';
import { getAllPages$ } from './getPages';
import { getPage$ } from './getPage';
import { MergeRequest } from '../types';

export const getMergeRequests$ = (
  createdAfter: Moment,
  createdBefore: Moment,
): Observable<MergeRequest> =>
  getAllPages$({
    method: 'GET',
    url: `/projects/${process.env.GITLAB_PROJECT_ID}/merge_requests`,
    params: {
      created_after: createdAfter.toISOString(),
      created_before: createdBefore.toISOString(),
      per_page: 100, // More than 100 is treated as 100
    },
  }).pipe(
    concatAll(),
    concatMap((mr) =>
      getPage$(
        `/projects/${mr.project_id}/merge_requests/${mr.iid}/approvals`,
      ).pipe(map((approvals) => ({ ...mr, ...approvals.data }))),
    ),
    concatMap((mr) =>
      getPage$(
        `/projects/${mr.project_id}/merge_requests/${mr.iid}/discussions`,
      ).pipe(map((discussions) => ({ ...mr, discussions: discussions.data }))),
    ),
    concatMap((mr) =>
      getPage$(
        `/projects/${mr.project_id}/merge_requests/${mr.iid}/award_emoji`,
      ).pipe(map((emoji) => ({ ...mr, award_emoji: emoji.data }))),
    ),
    share(),
  );
