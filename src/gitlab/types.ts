import { Observable } from 'rxjs';

export interface MergeRequest {
  iid: number;
  author: { name: string };
  merged_by?: { name: string };
  assignee?: { name: string };
  assignees: { name: string }[];
  suggested_approvers: { name: string }[];
  approvers: { user: { name: string } }[];
  approved_by: { user: { name: string } }[];
  award_emoji: { user: { name: string } }[];
  discussions: { notes: { author: { name: string } }[] }[];
}

export interface UserStats {
  User: string;
  Count: number;
}

export type StatsTable = UserStats[];

export interface SheetDefinition {
  title: string;
  countColumnTitle: string;
  stream: Observable<StatsTable>;
}
