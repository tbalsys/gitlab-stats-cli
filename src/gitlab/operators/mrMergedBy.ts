import { OperatorFunction } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrMergedBy = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    filter((mr) => !!mr.merged_by),
    map((mr) => (mr.merged_by ? mr.merged_by.name : 'null')),
    toStatsTable(),
  );
