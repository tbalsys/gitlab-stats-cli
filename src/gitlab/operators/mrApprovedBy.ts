import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { StatsTable, MergeRequest } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrApprovedBy = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    concatMap((mr) => mr.approved_by),
    map((approver) => approver.user.name),
    toStatsTable(),
  );
