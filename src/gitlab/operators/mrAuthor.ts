import { OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrAuthor = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    map((mr) => mr.author.name),
    toStatsTable(),
  );
