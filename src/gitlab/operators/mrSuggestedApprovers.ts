import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrSuggestedApprovers = (): OperatorFunction<
  MergeRequest,
  StatsTable
> => (input$) =>
  input$.pipe(
    concatMap((mr) => mr.suggested_approvers),
    map((approver) => approver.name),
    toStatsTable(),
  );
