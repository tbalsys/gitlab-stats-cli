import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrEmoji = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    concatMap((mr) => mr.award_emoji),
    map((emoji) => emoji.user.name),
    toStatsTable(),
  );
