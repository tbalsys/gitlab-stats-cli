import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrAssignees = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    concatMap((mr) => mr.assignees),
    map((assignee) => assignee.name),
    toStatsTable(),
  );
