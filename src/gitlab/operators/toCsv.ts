import { OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { StatsTable } from '../types';

export const toCsv = (): OperatorFunction<
  StatsTable,
  { csv: string; length: number }
> => (input$) =>
  input$.pipe(
    map((users: any[]) => {
      const columns = Object.keys(
        users.reduce((acc: Record<string, boolean>, user) => {
          for (const column of Object.keys(user)) {
            acc[column] = true;
          }

          return acc;
        }, {}),
      );
      const delimiter = ',';
      const rows = [columns.join(delimiter)];
      for (const user of users) {
        const csvRow = [];
        for (const column of columns) {
          const value = user[column] || '';
          csvRow.push(
            typeof value === 'string'
              ? `"${value.replace(/"/g, '""')}"`
              : String(value),
          );
        }

        rows.push(csvRow.join(delimiter));
      }

      return { csv: rows.join('\n'), length: rows.length };
    }),
  );
