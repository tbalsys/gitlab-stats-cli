import { OperatorFunction } from 'rxjs';
import { map, scan, takeLast, startWith } from 'rxjs/operators';
import { StatsTable } from '../types';

const toSortedArray = (counts: Record<string, number>) => {
  const data = Object.entries(counts).map(([User, Count]) => ({
    User,
    Count,
  }));
  data.sort((a, b) => b.Count - a.Count);

  return data;
};

const toSumsObject = (count: Record<string, number>, name: string) => {
  count[name] = count[name] ? count[name] + 1 : 1;

  return count;
};

export const toStatsTable = (): OperatorFunction<string, StatsTable> => (
  input$,
) =>
  input$.pipe(
    scan(toSumsObject, {}),
    startWith({}),
    takeLast(1),
    map(toSortedArray),
  );
