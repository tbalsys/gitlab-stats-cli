import { OperatorFunction } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrReviews = (): OperatorFunction<MergeRequest, StatsTable> => (
  $input,
) =>
  $input.pipe(
    concatMap((mr) => {
      const reviewers: Record<string, boolean> = {};

      for (const { user } of mr.approved_by) {
        reviewers[user.name] = true;
      }

      for (const { user } of mr.award_emoji) {
        reviewers[user.name] = true;
      }

      for (const { notes } of mr.discussions) {
        for (const { author } of notes) {
          reviewers[author.name] = true;
        }
      }

      if (reviewers[mr.author.name]) {
        delete reviewers[mr.author.name];
      }

      return Object.keys(reviewers);
    }),
    toStatsTable(),
  );
