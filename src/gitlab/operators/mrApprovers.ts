import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrApprovers = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    concatMap((mr) => mr.approvers),
    map((approver) => approver.user.name),
    toStatsTable(),
  );
