import { OperatorFunction } from 'rxjs';
import { map, concatMap } from 'rxjs/operators';
import { MergeRequest, StatsTable } from '../types';
import { toStatsTable } from './toStatsTable';

export const mrDiscussions = (): OperatorFunction<MergeRequest, StatsTable> => (
  input$,
) =>
  input$.pipe(
    concatMap((mr) => mr.discussions),
    concatMap((discussion) => discussion.notes),
    map((note) => note.author.name),
    toStatsTable(),
  );
