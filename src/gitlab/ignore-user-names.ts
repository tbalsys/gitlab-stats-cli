import { StatsTable } from './types';

export function ignoreUserNames(userNamesToIgnore: string[]) {
  const userNamesToIgnoreSet = new Set(userNamesToIgnore);

  return (statsTable: StatsTable): StatsTable =>
    statsTable.filter(({ User }) => !userNamesToIgnoreSet.has(User));
}
