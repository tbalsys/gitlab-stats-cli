interface LinksMap {
  [key: string]: string;
}

const trimQuotes = (value: string) => value.replace(/^"?(.*?)"?$/g, '$1');

const trimChevrons = (href: string) =>
  href.replace(/^[ <]+([^>]+)[ >]+/g, '$1');

export function parseLinkHeader(linkHeader: string): LinksMap {
  if (!linkHeader) {
    return {};
  }

  return linkHeader.split(',').reduce((res: LinksMap, link: string) => {
    const [href, rel] = link.split(';');
    if (rel) {
      const [, key] = rel.split('=');
      if (key) {
        res[trimQuotes(key)] = trimChevrons(href);
      }
    }

    return res;
  }, {});
}
