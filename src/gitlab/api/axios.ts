import axios from 'axios';

if (!process.env.GITLAB_INSTANCE) {
  console.error('Environment variable GITLAB_INSTANCE is not set');
  process.exit(1);
}

if (!process.env.GITLAB_ACCESS_TOKEN) {
  console.error('Environment variable GITLAB_ACCESS_TOKEN is not set');
  process.exit(1);
}

const instance = axios.create({
  baseURL: `${process.env.GITLAB_INSTANCE}/api/v4`,
  headers: {
    'Private-Token': process.env.GITLAB_ACCESS_TOKEN,
  },
});

instance.interceptors.request.use(
  (config) => {
    console.log(`GET ${instance.defaults.baseURL}${config.url}`);

    return config;
  },
  (error) => Promise.reject(error),
);

instance.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response && error.response.data) {
      console.error(error.response.data.message);
    }

    return Promise.reject(error);
  },
);

export default instance;
