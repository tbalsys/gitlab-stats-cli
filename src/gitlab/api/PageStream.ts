import { Readable } from 'stream';
import { AxiosRequestConfig } from 'axios';
import axios from './axios';
import { getPage } from './getPage';

class PageStream extends Readable {
  private next: string | undefined;

  constructor(
    requestParams: AxiosRequestConfig,
    options?: { [key: string]: string },
  ) {
    super({
      objectMode: true,
      ...options,
    });

    console.log('GitLab query: ', requestParams);
    this.next = axios.getUri(requestParams);
  }

  public async _read() {
    if (!this.next) {
      this.push(null);
    } else {
      try {
        const { data, next } = await getPage(this.next);
        this.next = next;
        this.push(data || []);
      } catch (error) {
        this.destroy(error);
      }
    }
  }
}

export default PageStream;
