import axios from './axios';
import { parseLinkHeader } from './parseLinkHeader';

export async function getPage(
  url: string,
): Promise<{
  data: any[];
  next: string | undefined;
}> {
  try {
    const response = await axios.get(url);
    const { next } = parseLinkHeader(response.headers.link);

    return {
      data: response.data,
      next,
    };
  } catch (error) {
    console.log(
      `request to ${axios.defaults.baseURL}${url} failed, reason: ${error.message}`,
    );

    return {
      data: [],
      next: undefined,
    };
  }
}
