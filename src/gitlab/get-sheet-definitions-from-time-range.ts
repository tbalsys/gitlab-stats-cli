import { Moment } from 'moment';

import { getCommits$, getMergeRequests$ } from './observables';
import {
  mrAuthor,
  mrReviews,
  mrApprovedBy,
  mrMergedBy,
  mrAssignees,
  mrDiscussions,
  mrEmoji,
} from './operators';
import { SheetDefinition } from './types';
import { map } from 'rxjs/operators';
import { ignoreUserNames } from './ignore-user-names';

export function getSheetDefinitionsFromTimeRange(
  createdAfterDate: Moment,
  createBeforeDate: Moment,
  userNamesToIgnore: string[] = [],
): SheetDefinition[] {
  const mr$ = getMergeRequests$(createdAfterDate, createBeforeDate);

  const sheetDefinitions: SheetDefinition[] = [
    {
      title: 'Commits',
      countColumnTitle: 'Commits made',
      stream: getCommits$(createdAfterDate, createBeforeDate),
    },
    {
      title: 'MR authors',
      countColumnTitle: 'MRs created',
      stream: mr$.pipe(mrAuthor()),
    },
    {
      // Comments, approvers, emoji
      title: 'MR reviewers',
      countColumnTitle: 'MRs reviewed',
      stream: mr$.pipe(mrReviews()),
    },
    {
      title: 'MR approved by',
      countColumnTitle: 'MRs approved',
      stream: mr$.pipe(mrApprovedBy()),
    },
    {
      title: 'MR merged by',
      countColumnTitle: 'MRs merged',
      stream: mr$.pipe(mrMergedBy()),
    },
    {
      title: 'MR assignees',
      countColumnTitle: 'Assigned to MRs',
      stream: mr$.pipe(mrAssignees()),
    },
    {
      title: 'MR discussions',
      countColumnTitle: 'MRs commented on',
      stream: mr$.pipe(mrDiscussions()),
    },
    {
      title: 'MR award emoji',
      countColumnTitle: 'MRs reacted to (emoji)',
      stream: mr$.pipe(mrEmoji()),
    },
  ];

  const ignoreUserNamesMapper = ignoreUserNames(userNamesToIgnore);

  return sheetDefinitions.map(({ stream, ...sheetDefinition }) => ({
    ...sheetDefinition,
    stream: stream.pipe(map(ignoreUserNamesMapper)),
  }));
}
