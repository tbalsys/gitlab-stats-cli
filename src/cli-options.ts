import * as moment from 'moment';
import { Argv } from 'yargs';

export interface SharedCLIOptions {
  fromDate: moment.Moment;
  toDate: moment.Moment;
  ignoreUserNames: string[] | undefined;
}

export const sharedCLIOptionsBuilder = (
  options: Argv,
): Argv<SharedCLIOptions> =>
  options
    .option('fromDate', {
      alias: ['f', 'from'],
      default: moment().startOf('month'),
      coerce: moment,
      description: 'Data will be extracted for MRs created after this date',
    })
    .option('toDate', {
      alias: ['t', 'to'],
      default: moment(),
      coerce: moment,
      description: 'Data will be extracted for MRs created before this date',
    })
    .array('ignoreUserNames')
    .describe(
      'ignoreUserNames',
      'Discards information about specified user names in the final stats. Useful to ignore CI',
    )
    .coerce(
      'ignoreUserNames',
      (userNames: unknown[] = []) =>
        userNames.filter(
          (userName) => typeof userName === 'string',
        ) as string[],
    );
