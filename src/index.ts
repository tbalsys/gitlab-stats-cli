import * as moment from 'moment';
import * as yargs from 'yargs';
import { extractStatsToGoogleSheets } from './google';
import { sharedCLIOptionsBuilder } from './cli-options';
import { xlsxCLIOptionsBuilder, extractStatsToXlsx } from './xlsx';

(moment as any).defaultFormat = 'YYYY-MM-DD HH:mmZ';

if (!process.env.GITLAB_PROJECT_ID) {
  console.error('Environment variable GITLAB_PROJECT_ID is not set');
  process.exit(1);
}

yargs
  .command(
    'xlsx',
    'Export data to an XLSX file',
    xlsxCLIOptionsBuilder,
    extractStatsToXlsx,
  )
  .command(
    'google',
    'Export data to Google Sheets',
    sharedCLIOptionsBuilder,
    extractStatsToGoogleSheets,
  )
  .demandCommand()
  .parse();
