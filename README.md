# Description

Command line utility to generate statistics about one GitLab project and save it as an XLSX file or
directly to a Google Spreadsheet.

## Installation

```sh
npm install
```

Create configuration file

```sh
cp .env.dist .env
```

Customize environment variables as needed for GitLab instance, project id, etc., see next sections
for hints.

Variables prefixed with `GOOGLE` are only required when exporting directly to Google Sheets.

## GitLab permissions

1. Login to your GitLab instance. It would be good to have a separate account for this application.
2. On top-right corner click your avatar → click **Settings** → **Access Tokens** → enter _name_ →
   select **api** checkbox → click **Create personal access token** → copy generated token to
   `GITLAB_ACCESS_TOKEN` environment variable in `.env` json.

## Google permissions

This is only necessary when exporting the stats directly to Google Sheets. Alternatively, export
the stats to an XLSX file.

1. Go to https://console.cloud.google.com and login.
2. Go to **IAM & admin** → **Manage resources** → **Create Project**, Enter name for a project,
   active newly created project in top navbar.
3. Go to **APIs & Services** → **Library** → search for and click **Google Drive API** → click
   **Enable**
4. Go to **APIs & Services** → **Library** → search for and click **Google Sheets API** → click
   **Enable**
5. Go to **APIs & Services** → **Credentials** → Click **Create credentials** → **Service account
   key** → **New service account** → enter _name_ and _id_, select _project/browser_ role, _json_
   key type → click **Create** → credentials json file will be downloaded.
6. From credentials json file values into `.env` file:

   - `client_email` to `GOOGLE_CLIENT_EMAIL`
   - `private_key` to `GOOGLE_PRIVATE_KEY`

7. Go to https://drive.google.com
8. **Share** a folder where you want output spreadsheets to to be uploaded → enter email of created
   service account (`client_email` in credentials json file) → click **Send** → confirm by clicking
   **Yes**. Failing to share parent folder in Google Drive will conclude with an application error:
   `Internal error encountered.`
9. Enter this folder, copy folder id for browser URL and paste it into `GOOGLE_PARENT_FOLDER_ID` in
   `.env` file.

## Usage

To use the tool, invoke the following command:

```sh
npm start -- [arguments here]
```

Remember about the double dash (`--`) between `npm start` and the arguments.

For help, use:

```sh
npm start -- --help
```

### Exporting to XLSX

For help:

```sh
npm start -- xlsx --help
```

Example:

```sh
npm start -- xlsx --fromDate 2020-07-31T22:00:00.000Z --toDate 2020-08-13T09:52:12.492Z --file stats.xlsx
```

Keep in mind `fromDate`, `toDate` and `file` arguments are optional. See defaults by invoking the
`--help`.

### Exporting to Google Sheets

For help:

```sh
npm start -- google --help
```

Example:

```sh
npm start -- google --fromDate 2020-07-31T22:00:00.000Z --toDate 2020-08-13T09:52:12.492Z
```

Keep in mind `fromDate`, `toDate` and `file` arguments are optional. See defaults by invoking the
`--help`.

### Additional options

#### `ignoreUserNames`

A way to exclude specified user names in the final stats. Useful when there is a user for the CI
that reacts to MRs or posts comments with CI status.

The name has to be equal to the user's display name (case-sensitive).

```sh
npm start -- xlsx --ignoreUserNames CI "CI Builder" --fromDate 2020-07-31T22:00:00.000Z
```
